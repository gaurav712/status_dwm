# status\_dwm
a simple statusbar(dwm version);

It's a (kind-of) fork of https://gitlab.com/gaurav712/status. Only difference is that instead of **stdout**, it sets X11's root window's title.

# NOTICE
It is no longer maintained! Have a look at this: https://gitlab.com/gaurav712/status
