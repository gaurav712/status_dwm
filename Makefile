CC="gcc"
CFLAGS="-lX11" "-Wall"

all:
	$(CC) -o status_dwm status_dwm.c $(CFLAGS)

clean:
	rm -f status_dwm

install: all
	cp status_dwm /usr/local/bin/status_dwm

uninstall:
	rm -f /usr/local/bin/status_dwm
