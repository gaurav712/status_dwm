/*
 * Simple statusbar showing battery percentage, date, time, wifi-status and network upload and download speed.
 *
 * Copyright (c) 2019 Gaurav Kumar Yadav <gaurav712@protonmail.com>
 * for license and copyright information, see the LICENSE file distributed with this source
 *
 * It's dwm's(dwm.suckless.org) version of my original status program. It doesn't throw up stuff on stdout but sets X's root window's name instead.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <string.h>
#include <X11/Xlib.h>

/* function to copy Num elements from n of a string to another */
void strNumFromNCpy(char *dest, char *src, short n, short num);
/* explains itself */
void setRootWindowName(char *str);
/* concatenates dnld and upld speed to status(it's a mess as they're numbers) */
void catDownloadUpload(unsigned down, unsigned up, char *status);
/* reverses a string */
void strReverse(char *str, unsigned short len);

static Display *dis;

int main(void) {

    unsigned long up1, up2, down1, down2;
    FILE *up, *down;
    char temp[50], status[100], wlan_stat;
    time_t current_time;

    while(1) {

        /* Reset status variable */
        strcpy(status, "| ");

        /* BATTERY PERCENTAGE */
        up = fopen("/sys/class/power_supply/BAT1/capacity", "r");
        fscanf(up, "%s", temp);
        fclose(up);
        strcat(status, temp);
        strcat(status, "%(");


        /* BATTERY STATUS */
        up = fopen("/sys/class/power_supply/BAT1/status", "r");
        fscanf(up, "%s", temp);
        fclose(up);
        strcat(status, temp);
        strcat(status,") | ");

        /* DATE */
        current_time = time(NULL);
        strNumFromNCpy(temp, ctime(&current_time), 0, 10);
        strcat(status, temp);
        strcat(status, " | ");

        /* TIME */
        strNumFromNCpy(temp, ctime(&current_time), 11, 8);
        strcat(status, temp);

        /* RFKILL */
        up = fopen("/sys/class/rfkill/rfkill0/state", "r");
        fscanf(up, "%c", &wlan_stat);
        fclose(up);

        if(wlan_stat == '1') {

            /* WIFI STATUS(connected or not) */
            up = fopen("/sys/class/net/wlp3s0/operstate", "r");
            fscanf(up, "%c", &wlan_stat);
            fclose(up);
            if(wlan_stat == 'u') {

                /* UPLOAD AND DOWNLOAD SPEED */
                up = fopen("/sys/class/net/wlp3s0/statistics/tx_bytes", "r");
                down = fopen("/sys/class/net/wlp3s0/statistics/rx_bytes", "r");
                fscanf(up, "%ld", &up1);
                fscanf(down, "%ld", &down1);
                sleep(1);
                /* Moving the pointer again to the begining */
                rewind(up);
                rewind(down);
                fscanf(up, "%ld", &up2);
                fscanf(down, "%ld", &down2);
                fclose(up);
                fclose(down);
                catDownloadUpload((down2 - down1)/1024, (up2 - up1)/1024, status);
                setRootWindowName(status);

            } else {
                strcat(status, " | Disconnected");
                setRootWindowName(status);
                sleep(1);
            }

        } else {
            strcat(status, " | WLAN_OFF");
            setRootWindowName(status);
            sleep(1);
        }
    }

    return 0;
}

void
strNumFromNCpy(char *dest, char *src, short n, short num){

    short count = 0;
    for(; count < num; count++){
        dest[count] = src[n];
        n++;
    }
    dest[count] = '\0';
}

void setRootWindowName(char *str) {

    if(!(dis = XOpenDisplay(NULL))) {
        perror("Can't open display!");
        exit(1);
    }

    XStoreName(dis, DefaultRootWindow(dis), str);
    XCloseDisplay(dis);
}

void catDownloadUpload(unsigned down, unsigned up, char *status) {

    char temp[20];
    unsigned short count;

    strcat(status, " | ");

    /* Download Speed */
    if(!down) {
        strcat(status, "0kb/s ");
    } else {
        for(count = 0; down != 0; count++) {
            temp[count] = (down % 10) + '0';
            down /= 10;
        }
        temp[count] = '\0';
        strReverse(temp, count);
        strcat(status, temp);
        strcat(status, "kb/s ");
    }

    /* Upload Speed */
    if(!up) {
        strcat(status, "0kb/s");
    } else {
        for(count = 0; up != 0; count++) {
            temp[count] = (up % 10) + '0';
            up /= 10;
        }
        temp[count] = '\0';
        strReverse(temp, count);
        strcat(status, temp);
        strcat(status, "kb/s");
    }
}

void strReverse(char *str, unsigned short len) {

    char temp[20];
    unsigned short lenBak = len, count;

    strcpy(temp, str);

    len--;
    for(count = 0; count < lenBak; count++) {
        temp[count] = str[len];
        len--;
    }
    temp[count] = '\0';
    strcpy(str, temp);
}

